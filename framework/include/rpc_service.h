/*
 * MIT License
 * 
 * Copyright (c) 2019 极简美 @ konishi5202@163.com
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef __RPC_SERVICE_H__
#define __RPC_SERVICE_H__
#ifdef __cplusplus
extern "C" {
#endif
#include "cjson/cJSON.h"

typedef cJSON *(*erpc_service_callback_t)(cJSON *params);

/**********************************************************
 * description: register a service
 * parameter : module , the module name of service belong to;
 *             service, the name of service;
 *             handler, the pointer point to service;
 * return : 0 , register ok;
 *         -1 , register failed;
***********************************************************/
extern int erpc_service_register(const char *module, const char *service, erpc_service_callback_t handler);

/**********************************************************
 * description: unregister a service
 * parameter : module , the module name of service belong to;
 *             service, the name of service;
 * return : 0 , unregister ok;
 *         -1 , unregister failed;
***********************************************************/
extern int erpc_service_unregister(const char *module, const char *service);


/**********************************************************
 * description: call a ( remote ) service
 * parameter : module , the module name of service belong to;
 *             service, the name of service;
 *             params, the params of remote service;
 *             result, the response of remote service;
 *             tv , the max response time of remote service;
 * return : 0 , remote call ok;
 *         -1 , remote call failed;
***********************************************************/
extern int erpc_service_proxy_call(const char *module, const char *service, cJSON *params, cJSON **result, struct timeval *timeout);

#ifdef __cplusplus
}
#endif

#endif  // __RPC_SERVICE_H__

